class LinkedList:
    def __init__(self) -> None:
        self.length = 0
        self.head = None

    def isEmpty(self):
        return self.head is None

    def thowEmptyError(self):
        raise Exception('This list is Empty')
    
    def map(self, callback):
        currentNode = self.head
        index = 0
        while currentNode.next is not None:
            if callable(callback):
                callback(self, currentNode, index)
            currentNode = currentNode.next
            index += 1
    
    def getFirstNodeData(self):
        if self.isEmpty() is True:
            self.thowEmptyError()
        else:
            return self.head.data
    
    def getLastNodeData(self):
        if self.isEmpty() is True:
            self.thowEmptyError()
        else:
            currentNode = self.head
            while currentNode.next is not None:
                if currentNode.next is None:
                    return currentNode.data
                else:
                    currentNode = currentNode.next
            return None

    def removeFirstNode(self):
        if self.isEmpty() is True:
            self.thowEmptyError()
        else:
            tempHead = self.head
            self.head = tempHead.next
            self.length -= 1
            return tempHead
    
    def removeAt(self, nodeToDetele, startFrom = None):
        if self.length > 0:
            if startFrom is None:
                startFrom = self.head
            
            if startFrom.data is nodeToDetele.data:
                self.removeFirstNode()
                return True
            else:
                prevNode = startFrom
                while prevNode.data is not None & prevNode.data is not nodeToDetele.data:
                    prevNode = prevNode.next
                if prevNode.next is None:
                    raise Exception("Node to delete is not found!")
                prevNode.next = prevNode.next.next
                self.length -= 1
                return True
        return False
            
    
    def insertFirstNode(self, data):
        self.length += 1
        self.head = LinkedListNode(data, None)

    def insertNodeAtEnd(self, data):
        if self.head is None:
            self.head = LinkedListNode(data, None)
        else:
            currentNode = self.head
            for i in range(0, self.length):
                if currentNode.next is None:
                    self.length += 1
                    currentNode.next = LinkedListNode(data, None)
                    return True
                currentNode = currentNode.next
                i += 1

class LinkedListNode:
    def __init__(self, data, node):
        self.data = data
        self.next = node

ll = LinkedList()
tempArr = [ 3,4,5,6,7,8,0,4 ]
for temp in tempArr:
    if ll.length == 0:
        ll.insertFirstNode(temp)
    else:
        print(ll.insertNodeAtEnd(temp))
print('running Linked List Class test with length => ' + str(ll.length))
def mapIt(self, curNode, i): print("LinkedList Item => " + str(curNode.data) + " at " + str(i))
ll.map(mapIt)

# validating error case
llError = LinkedList()
print(llError.getFirstNodeData())