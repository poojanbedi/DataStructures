class LinkedList {
  constructor() {
    this.head = null
    this.length = 0
  }

  #throwEmptyError() {
    throw new Error('This list is Empty')
  }

  isEmpty() {
    return this.head === null
  }
  
  map(callback) {
    let currentNode = this.head;
    let index = 0;
    for(; index < this.length; index += 1) {
      if(typeof callback === 'function') {
        callback.call(this, currentNode, index);
      }
      currentNode = currentNode.next;
    }
  }

  getFirstNodeData() {
    if(this.isEmpty) {
      this.#throwEmptyError();
    } else {
      return this.head.data;
    }
  }

  getLastNodeData() {
    if(this.isEmpty) {
      this.#throwEmptyError();
    } else {
      let currentNode = this.head;
      while(currentNode.next === null) {
        currentNode = currentNode.next;
      }
      return currentNode.data;
    }
  }

  removeFirstNode() {
    if(this.head !== null) {
      const headData = this.getFirstNodeData();
      this.head = this.head.next;
      this.length -= 1;
      return headData;
    } else {
      this.#throwEmptyError()
    }
  }

  removeAt(startFrom = null, nodeToDetele) {
    if( this.length > 0 ) {
      if(startFrom === null) startFrom = this.head;

      if(startFrom.data === nodeToDetele.data) {
        this.removeFirstNode();
      } else {
        let previousNode = startFrom;
        while(previousNode.data !== null && previousNode.data !== nodeToDetele.data) {
          previousNode = previousNode.next;
        }

        if(previousNode.next === null) {
          this.errors.empty();
          return;
        }

        previousNode.next = previousNode.next.next;

      }
    }
  }

  insertFirstNode(item) {
    this.length += 1;
    this.head = new LinkedListNode(item, this.head);
  }

  insertNodeAtEnd(item) {
    if(this.head === null) {
      this.insertFirstNode(item);
    } else {
      let currentNode = this.head;
      while(currentNode.next !== null) {
        currentNode = currentNode.next;
      }

      currentNode.next = new LinkedListNode(item, null);
      this.length += 1;
    }
  }

}

class LinkedListNode {
  constructor(data, node) {
    this.data = data
    this.next = node
  }
}

/**
 * ------- Some unit test code  ---------
    const ll = new LinkedList();
    const aa = [ 3,4,5,6,7,8,0,4 ];
    for(let i =0; i<aa.length;i += 1) {
      if(ll.length === 0) {
        ll.insertFirstNode(aa[i]);
      } else {
        ll.insertNodeAtEnd(aa[i]);
      }
    }

    console.log("actual likedlist", ll);
    ll.map((item, index) => {
      console.log(item, ' at ' + index);
    })

    const newLL = new LinkedList()
    console.log(newLL.getFirstNodeData()) // Throw error case
 *  
 */